#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys


class SIPRequest:

    def __init__(self, data):
        self.data = data

    def parse(self):
        received = self.data.decode('utf-8')
        self._parse_command(received)
        primer_nl = received.splitlines()[1:]
        self._parse_headers(primer_nl[0])


    def _get_address(self, uri):
        cut = uri.split(":")
        address = cut[1]
        schema = cut[0]
        return address, schema

    def _parse_command(self, line):
        cut = line.split(' ')
        self.command = cut[0].upper()
        self.uri = cut[1]
        sip_uri = self._get_address(self.uri)
        self.address = sip_uri[0]
        schema = sip_uri[1]
        if self.command == "REGISTER":

            if schema == "sip":
                self.result = "200 OK"
            else:
                self.result = "416 Unsupported URI Scheme"
        else:
            self.result = "405 Method Not Allowed"

    def _parse_headers(self, first_nl):
        self.headers = {}
        for line in first_nl:
            split_line = line.split(':')
            if len(split_line) >= 2:
                cut = line.split(':')
                head = cut[0]
                value = cut[1]
                self.headers[head] = value


class SIPRegisterHandler(socketserver.BaseRequestHandler):
    """
    Echo server class
    """
    registro = {}
    def process_register(self, elemento):

        self.registro[self.client_address[0]] = elemento.headers

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        data = self.request[0]
        sock = self.request[1]
        sip_request = SIPRequest(data)
        sip_request.parse()

        if (sip_request.command == "REGISTER") and (sip_request.result == "200 OK"):
            self.process_register(sip_request)
        sock.sendto(f"SIP/2.0 {sip_request.result}\r\n\r\n".encode(), self.client_address)


def main():
    PORT = sys.argv[1]

    if len(sys.argv) < 2:
        sys.exit("Error de valores")

    try:
        serv = socketserver.UDPServer(('', int(PORT)), SIPRegisterHandler)
        print(f"Server listening in port {PORT}")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()

